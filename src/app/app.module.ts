import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ReservingComponent } from './reserving/reserving.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTabsModule, } from '@angular/material/tabs';
import { MatDatepickerModule } from '@angular/material';
import { MatButtonModule, MatFormFieldModule,MatNativeDateModule,MatInputModule} from '@angular/material';
import {MatRadioModule} from '@angular/material/radio';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';
import { ReactiveFormsModule } from '@angular/forms';
import {MatTableModule} from '@angular/material/table';
import {MatSelectModule} from '@angular/material/select';

@NgModule({
  declarations: [
    AppComponent,
    ReservingComponent
    

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatTabsModule, MatDatepickerModule, MatButtonModule, MatFormFieldModule,MatRadioModule,MatToolbarModule,MatCardModule,
    MatNativeDateModule,MatInputModule,ReactiveFormsModule,MatTableModule,MatSelectModule
  
  ],
  exports: [
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
