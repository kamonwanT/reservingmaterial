import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ReservingComponent} from './reserving/reserving.component';

const routes: Routes = [
  {path:'', redirectTo:'/reserving', pathMatch: 'full'},
  {path:'reserving', component:ReservingComponent},




];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
